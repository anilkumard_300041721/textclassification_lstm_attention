import gensim
from gensim.models import KeyedVectors
from datetime import datetime
import numpy as np
import tensorflow as tf
import random
import pandas as pd
import re
random.seed(123)
from spellchecker import SpellChecker
spellCorrector = SpellChecker()
import pickle

trainPercent = 0.8
wordEmbeddingSize = 200


# In[8]:

def getVocab(data):
    sensList = data['cleaned_feedback'].tolist()
    vocabDict = {}
    for sen in sensList:
        spSen = sen.split()
        for word in spSen:
            if word not in vocabDict:
                vocabDict[word] = len(vocabDict)
    vocabDict['<padWord>'] = len(vocabDict)
    vocabDict['<UNK>'] = len(vocabDict)
    return vocabDict

maxSenLen = 100
train_df = pd.read_csv("train_data.csv") #pre-processed training data
test_df = pd.read_csv("test_data.csv") #pre-processed test data

#with open('vocab_dict.pkl', 'rb') as vocab_dictF:
#    vocabDict = pickle.load(vocab_dictF)
vocabDict = getVocab(train_df)
print("Len of Vocab : ",len(vocabDict))
with open('WVsDict.pkl', 'rb') as WVsDictFile:  #pre-trained word vectors
    WVsDict = pickle.load(WVsDictFile)
print(train_df.shape)
print(test_df.shape)
print(len(vocabDict))
print(len(WVsDict))


# In[10]:


#print(rev_logistics_data.head(1)['cleaned_feedback'])
level1Labels = train_df.Dept_2.unique()
level2Labels = train_df.Sub_Dept_2.unique()
L1LabelsDict = {}
L2LabelsDict = {}
for value in level1Labels:
    L1LabelsDict[value] = len(L1LabelsDict)
for value2 in level2Labels:
    L2LabelsDict[value2] = len(L2LabelsDict)
print(L1LabelsDict)
nofClassesInL1 = len(L1LabelsDict)
nofClassesInL2 = len(L2LabelsDict)
print(nofClassesInL1,nofClassesInL2)


# In[12]:


def getEncodedData(batch_data_df,vocabDict,L1LabelsDict,L2LabelsDict,WVsDict,isTrain,maxSenLen):
    nofSens = batch_data_df.shape[0]
    maxSenLenBatch = maxSenLen #getMaxSenLen(batch_data_df)
    encodedData = np.zeros(shape=[nofSens,maxSenLenBatch])
    nofClassesInL1 = len(L1LabelsDict)
    nofClassesInL2 = len(L2LabelsDict)
    encodedLabelsL1 = np.zeros(shape=[nofSens,nofClassesInL1])
    encodedLabelsL2 = np.zeros(shape=[nofSens,nofClassesInL2])
    curSenIndex = 0
    for index, row in batch_data_df.iterrows():
        curSen = row['cleaned_feedback']
        curSpSen = curSen.split()
        curSenLen = len(curSpSen)
        curLabelL1 = row['Dept_2']
        curLabelL2 = row['Sub_Dept_2']
        curLabelIndexL1 = L1LabelsDict[curLabelL1]
        curLabelIndexL2 = L2LabelsDict[curLabelL2]
        #print(nofSens,maxSenLenBatch,curLabelL1,curLabelIndexL1,curLabelL2,curLabelIndexL2,curSen)
        encodedLabelsL1[curSenIndex][curLabelIndexL1] = 1
        encodedLabelsL2[curSenIndex][curLabelIndexL2] = 1
        for i in range(min(curSenLen,maxSenLenBatch)):
            word = curSpSen[i]
            if word in vocabDict:
                if word in WVsDict:
                    encodedData[curSenIndex][i] = vocabDict[word]
                else:
                    encodedData[curSenIndex][i] = vocabDict['<UNK>']
            else:
                encodedData[curSenIndex][i] = vocabDict['<UNK>']
        while i < maxSenLenBatch:
            encodedData[curSenIndex][i] = vocabDict['<padWord>']
            i+=1
        curSenIndex+=1
    return encodedData,encodedLabelsL1,encodedLabelsL2,maxSenLenBatch


# In[13]:


vocSize = len(vocabDict)
gsInitVecs = np.array(np.zeros(shape=[vocSize,wordEmbeddingSize]))
for word,index in vocabDict.items():
    if word in WVsDict:
        gsInitVecs[index,:] = WVsDict[word]
    else:
        gsInitVecs[index,:] = np.zeros(shape=[wordEmbeddingSize])
    #gsInitVecs[index,:] = np.random.uniform(low=-1.0,high=1.0,size=[wordEmbeddingSize])


# In[19]:


def findSeqLength(sequence):
    used = tf.sign(tf.reduce_max(tf.abs(sequence), 2))
    length = tf.reduce_sum(used, 1)
    length = tf.cast(length, tf.int32)
    return length

#Hyperparameters
nofUnitsInH1 = 200
l2_reg_lambda = 0.0001
nofLstmUnits = 200

#def biAttModel(inputBatchData,inputBatchLabels):
tf.reset_default_graph()
l2_loss = tf.constant(0.0)
maxSenLenBatchT2 = maxSenLen
dropoutLstmLayer = tf.placeholder(tf.float32)
dropoutPenultimate = tf.placeholder(tf.float32)
X = tf.placeholder(tf.int64, shape=(None, maxSenLen))
W1 = tf.to_float(tf.Variable(gsInitVecs,name = "noreg1"),name = "noreg")
wemdsX = tf.nn.embedding_lookup(W1,X)
fcell = tf.contrib.rnn.LSTMCell(nofLstmUnits)
bcell = tf.contrib.rnn.LSTMCell(nofLstmUnits)
# Or LSTMCell(num_units) #BasicLSTMCell(nofUnits)
fcell = tf.contrib.rnn.DropoutWrapper(fcell, output_keep_prob=1.0 - dropoutLstmLayer)
bcell = tf.contrib.rnn.DropoutWrapper(bcell, output_keep_prob=1.0 - dropoutLstmLayer)
lengthsVec = findSeqLength(wemdsX)
Y1 = tf.placeholder(tf.float32, shape=(None, nofClassesInL1))
#Y2 = tf.placeholder(tf.float32, shape=(None, nofClassesInL2))
outputs, outputStates = tf.nn.bidirectional_dynamic_rnn(fcell,bcell, wemdsX, dtype=tf.float32,sequence_length=lengthsVec)

outputsTensor = tf.convert_to_tensor(outputs)
outputsTensor2 = tf.concat([outputsTensor[0], outputsTensor[1]], axis=2)#tf.reduce_max(outputsTensor, axis=0)
tempTens = tf.reshape(tf.transpose(outputsTensor2, [1, 2, 0]),shape=[2*nofLstmUnits*maxSenLenBatchT2,-1])

attW1 = tf.Variable(tf.truncated_normal(shape=[2*nofLstmUnits,1], stddev=0.1))
vu = tf.tensordot(outputsTensor2, attW1, axes=1, name='attention_dot')
alpha1V = tf.transpose(tf.nn.softmax(vu, name='alphas'),[0,2,1])
token1Vecs = tf.squeeze(tf.tanh(tf.matmul(a=alpha1V,b=outputsTensor2)),axis=1)
nofUnitsInTok1Hid = 100
token1HidWts = tf.Variable(tf.truncated_normal(shape=[2*nofLstmUnits,nofUnitsInTok1Hid], stddev=0.1))
token1HidWtsBias = tf.Variable(tf.constant(0.1, shape=[nofUnitsInTok1Hid]),name="token1HidWtsBias")
tok1HidLayerVec = tf.nn.relu(tf.nn.xw_plus_b(token1Vecs,token1HidWts,token1HidWtsBias))
opseqToken1Wts = tf.Variable(tf.truncated_normal(shape=[nofUnitsInTok1Hid,nofClassesInL1], stddev=0.1))
opseqToken1WtsBias = tf.Variable(tf.constant(0.1, shape=[nofClassesInL1]),name="Bias4")
opseqToken1Vec = tf.nn.xw_plus_b(tok1HidLayerVec,opseqToken1Wts,opseqToken1WtsBias)

losses = tf.nn.softmax_cross_entropy_with_logits(logits=opseqToken1Vec, labels=Y1)
for tf_var in tf.trainable_variables():
    if not ("noreg" in tf_var.name or "Bias" in tf_var.name):
        l2_loss += tf.nn.l2_loss(tf_var)
loss = tf.reduce_mean(losses) + l2_reg_lambda*l2_loss
global_step = tf.Variable(0, name="global_step", trainable=False)
optimizer = tf.train.AdamOptimizer(0.0005)
grads_and_vars = optimizer.compute_gradients(loss)
train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

predictionsOp1 = tf.argmax(opseqToken1Vec, axis=1)
correctPredictions = tf.equal(predictionsOp1, tf.argmax(Y1, 1))
accuracy = tf.reduce_mean(tf.cast(correctPredictions, "float"), name="accuracy")


# In[24]:


batchSize = 32
testBatchSize = 256
nofepochs = 20
nofBatches = int(train_df.shape[0]/batchSize)
nofTestbBatches = int(test_df.shape[0]/testBatchSize)
print("nofBatches :",nofBatches)
print("nofTestbBatches :",nofTestbBatches)
maxTestAcc = 0.0
saver = tf.train.Saver()
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for iterIndex in range(nofepochs):
        train_df = train_df.sample(frac=1, replace=False)
        trainAccs = []
        for batchIter in range(101): #nofBatches
            batchDataDf= train_df[batchIter*batchSize : batchIter*batchSize+batchSize]
            batchX,batchY1,batchY2,maxSenLenBatchD = getEncodedData(batchDataDf,vocabDict,L1LabelsDict,L2LabelsDict,WVsDict,1,maxSenLen)
            _,acc,losss = sess.run([train_op,accuracy,loss], feed_dict={X: batchX, Y1:batchY1, dropoutLstmLayer: 0.4, dropoutPenultimate: 0.3})
            trainAccs.append(acc)
            if batchIter % 1000 == 0:
                print(iterIndex, ",", batchIter, ",loss : ", losss,",acc :",acc ,"...........", str(datetime.now()), " ....")
        print("Train Accuracy : ",float(sum(trainAccs))/len(trainAccs))
        nofCorrectPreds = 0
        testAccs = []
        for testBatchIter in range(2): #nofTestbBatches
            batchDataDf= test_df[testBatchIter*testBatchSize : testBatchIter*testBatchSize+testBatchSize]
            batchX,batchY1,batchY2,maxSenLenBatchD = getEncodedData(batchDataDf,vocabDict,L1LabelsDict,L2LabelsDict,WVsDict,0,maxSenLen)
            acc,nofCorPredsBArr = sess.run([accuracy,correctPredictions], feed_dict={X: batchX, Y1:batchY1, dropoutLstmLayer: 0, dropoutPenultimate: 0})
            #print("nofCorPredsBArr :",nofCorPredsBArr)
            nofCorPredsB = nofCorPredsBArr.sum()
            nofCorrectPreds = nofCorrectPreds + nofCorPredsB
            testAccs.append(acc)
            #print(len(nofCorPredsBArr),nofCorPredsB,acc,nofCorrectPreds)
        #print(float(sum(testAccs))/len(testAccs), nofCorrectPreds/float(nofTestbBatches*testBatchSize))
        testAccuracy = float(sum(testAccs))/len(testAccs)
        print("iter : ",iterIndex, "Acc : ",testAccuracy, ".....",str(datetime.now()) ," ....")
        if testAccuracy > maxTestAcc:
                maxTestAcc = testAccuracy
                save_path = saver.save(sess, "bestModel/bestModel_biAtt")


# In[25]:


# with tf.Session() as sess:
#     sess.run(tf.global_variables_initializer())
#     print(sess.run(attW1))
#     saver1 = tf.train.import_meta_graph("bestModel/bestModel_biAtt.meta")
#     saver1.restore(sess,tf.train.latest_checkpoint('bestModel/'))
#     print(sess.run(attW1))

